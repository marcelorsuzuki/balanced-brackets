package balancedBrackets;

import java.lang.IllegalArgumentException;
import java.util.HashMap;
import java.util.Stack;


public class VerificadorBalanceamento {

    private Stack<String> brackets = new Stack<String>();
    private HashMap<String, String> conjuntoChavesBrackets = new HashMap<String, String>() {{
        put("{", "}");
        put("(", ")");
        put("[", "]");
        put("]", "");
        put(")", "");
        put("}", "");
    }};
    private String texto;

    public VerificadorBalanceamento(String texto) {
        if (texto == null || texto.isEmpty())
            throw new IllegalArgumentException("O Argumento precisa ser uma string valida!");
        this.texto = texto;
    }

    public Boolean balanceada() {
        for (char c : this.texto.toCharArray()) {
            String  caractere = String.valueOf(c);
            if (this.conjuntoChavesBrackets.containsKey(caractere))
                if (this.verificaBalanceamento(caractere).equals(""))
                	return false;
        }
        return this.brackets.empty();
    }

    private String verificaBalanceamento(String caractere) {
    	
        String bracketEsperado = this.conjuntoChavesBrackets.get(caractere);
        
        if (!this.brackets.isEmpty() && this.brackets.peek().equals(caractere))
            return this.brackets.pop();
        else
            return this.brackets.push(bracketEsperado);
    }

}
