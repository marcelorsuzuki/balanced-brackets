package balancedBrackets;

import java.lang.IllegalArgumentException;
import java.util.HashMap;
import java.util.Map.Entry;

import balancedBrackets.VerificadorBalanceamento;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class VerificadorBalanceamentoTest {
    String stringValida = "[{()}](){}";
    String stringInvalida = "[]{()";

    @Test
    void criaInstanciaVerificadorBalanceamentoComSucesso() {
        new VerificadorBalanceamento(this.stringValida);
    }

    @Test
    void tentaCriarInstanciaVerificadorBalanceamentoPassandoNullValueRecebeErro() {
        Assertions.assertThrows(
            IllegalArgumentException.class, () ->  new VerificadorBalanceamento(null));
    }

    @Test
    void tentaCriarInstanciaVerificadorBalanceamentoPassandoStringVaziaRecebeErro() {
        Assertions.assertThrows(
            IllegalArgumentException.class, () ->  new VerificadorBalanceamento(""));
    }

    @Test
    void recebeStringBalanceadaRetornaTrue() {
        VerificadorBalanceamento verificador = new VerificadorBalanceamento(this.stringValida);
        Assertions.assertTrue(verificador.balanceada());
    }

    @Test
    void recebeStringNaoBalanceadaRetornaFalse() {
        VerificadorBalanceamento verificador = new VerificadorBalanceamento(this.stringInvalida);
        Assertions.assertFalse(verificador.balanceada());
    }

    @Test
    void AnalisaUmConjuntoDeStringsComparandoVerificacaoBalanceamentoComEsperado() {
        HashMap<String, Boolean> conjunto_dados = new HashMap<String, Boolean>() {{
            put("(){}[]", Boolean.TRUE);
            put("[{()}](){}", Boolean.TRUE);
            put("[]{()", Boolean.FALSE);
            put("[{)]", Boolean.FALSE);
            
            put("))", Boolean.FALSE);
            put("]]", Boolean.FALSE);
            put(")", Boolean.FALSE);
            put("]", Boolean.FALSE);
            put("([)]", Boolean.FALSE);
        }};
        
        for (Entry<String, Boolean> dados : conjunto_dados.entrySet()) {
            String stringValidacao = dados.getKey();
            Boolean balanceamentoEsperado = dados.getValue();
            VerificadorBalanceamento verificador = new VerificadorBalanceamento(stringValidacao);
            Assertions.assertEquals(balanceamentoEsperado, verificador.balanceada());
        }
    }
}
