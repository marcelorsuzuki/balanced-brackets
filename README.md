# Balanced Brackets

Simple class to verified if a string is balanced or not.

## Getting Started

All the Test Cases described in the `pdf` are in `VerificadorBalanceamentoTest`.

### Prerequisites

 - Java 1.8
 - Maven 3.5.4
 - Make 4.1

### Installing

Installing dependencies:

```bash
:$ make install_dependencies
```

## Running the tests

Running TestCases:

```bash
:$ make tests
```
